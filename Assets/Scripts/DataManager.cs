using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataManager : SingletonBehaviour<DataManager>
{
    protected override void Init()
    {
        
    }

    public Sprite LoadItemSprite_spriteSheet(string brand, string type, int index)
    {
        string path = Path.Combine("SpriteSheet", brand, type);
        Sprite[] textures = Resources.LoadAll<Sprite>(path);
        if (textures.Length <= 0)
            return null;
        else
            return textures[index];
    }

    public Texture2D LoadItemTexture2D_spriteSheet(string brand, string type, int index)
    {
        string path = Path.Combine("SpriteSheet", brand, type);
        Sprite[] textures = Resources.LoadAll<Sprite>(path);
        if (textures.Length <= 0)
            return null;
        else
        {
            Sprite sprite = textures[index];
            Texture2D texture = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] colors = textures[index].texture.GetPixels((int)sprite.textureRect.x,
                                             (int)sprite.textureRect.y,
                                             (int)sprite.textureRect.width,
                                             (int)sprite.textureRect.height);
            texture.SetPixels(colors);
            texture.Apply();
            return texture;
        }
    }
}
