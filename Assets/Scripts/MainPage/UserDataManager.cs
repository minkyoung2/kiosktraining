using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UserDataManager : MonoBehaviour
{
    public static UserData userData = new UserData();
    public static UserDataManager instance = null;

    private void Awake()
    {
        if (instance == null) //instance가 null. 즉, 시스템상에 존재하고 있지 않을때
        {
            instance = this; //내자신을 instance로 넣어줍니다.
            DontDestroyOnLoad(gameObject); //OnLoad(씬이 로드 되었을때) 자신을 파괴하지 않고 유지
        }
        else
        {
            if (instance != this) //instance가 내가 아니라면 이미 instance가 하나 존재하고 있다는 의미
            Destroy(this.gameObject); //둘 이상 존재하면 안되는 객체이니 방금 AWake된 자신을 삭제
        }

        DataLoad();
    }

    public void DataLoad()
    {
        userData.isFirstStart = Convert.ToBoolean(PlayerPrefs.GetInt("isFirstStart", Convert.ToInt16(true))); // true=1, false=0
        
        if(!userData.isFirstStart)
        {
            string[] mcDataSet = PlayerPrefs.GetString("McProgress").Split(",");
            string[] lotteCinemaDataSet = PlayerPrefs.GetString("LotteCinemaProgress").Split(",");

            for(int i = 0; i < 3; i++)
            {
                userData.McProgress[i] = int.Parse(mcDataSet[i]);
                userData.LotteCinemaProgress[i] = int.Parse(lotteCinemaDataSet[i]);
            };
        }
        else
        {
            for(int i = 0; i < 3; i++)
            {
                userData.McProgress[i] = 0;
                userData.LotteCinemaProgress[i] = 0;
            };
            
            DataSave();
        }

        print(userData.McProgress[0]);
        print(userData.McProgress[1]);
        print(userData.McProgress[2]);
    }

    public void DataSave()
    {
        PlayerPrefs.SetInt("isFirstStart", Convert.ToInt16(userData.isFirstStart)); // true=1, false=0

        string mcDataSet = "";
        string lotteCinemaDataSet = "";
        for(int i = 0; i < 3; i++)
        {
            mcDataSet = mcDataSet + userData.McProgress[i].ToString() + ",";
            lotteCinemaDataSet = lotteCinemaDataSet + userData.LotteCinemaProgress[i].ToString() + ",";
        };
        
        mcDataSet = mcDataSet.TrimEnd(',');
        lotteCinemaDataSet = lotteCinemaDataSet.TrimEnd(',');
        
        PlayerPrefs.SetString("McProgress", mcDataSet);
        PlayerPrefs.SetString("LotteCinemaProgress", lotteCinemaDataSet);
    }

    private void OnApplicationQuit() //앱 종료 시 호출
    {
        DataSave();
    }

    // void OnApplicationPause(bool pause)
    // {
	//     if (pause) //앱이 비활성화 일 때
	//     {
    //         DataSave();
    //     }
    //     else
	//     {
    //         DataLoad();
  	//     }
    // }


    //test
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.G))
        {
            userData.McProgress[0] = 100;
            userData.McProgress[1] = 25;
            userData.McProgress[2] = 50;

            userData.LotteCinemaProgress[0] = 55;
            userData.LotteCinemaProgress[1] = 12;
            userData.LotteCinemaProgress[2] = 22;
            DataSave();
        }
    }
}

public class UserData
{
    //최초 실행 시에만 가이드 팝업
    public bool isFirstStart;

    //맥도날드 진행률(학습하기, 자유학습, 실전엽습 순서)
    public int[] McProgress = new int[3];

    
    //롯데시네마 진행률(학습하기, 자유학습, 실전엽습 순서)
    public int[] LotteCinemaProgress = new int[3];
}

