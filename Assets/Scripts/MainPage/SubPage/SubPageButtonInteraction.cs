using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SubPageButtonInteraction : MonoBehaviour
{
    public GameObject[] pageArray;
    public List<GameObject> categoryOnTxtList = new List<GameObject>();

    public void ButtonPress(int id)
    {
        print(id + "씬이동");
    }

    public void ToggleOn(int id)
    {
        for(int i = 0; i < categoryOnTxtList.Count; i++)
        {
            categoryOnTxtList[i].SetActive(false);   
        };

        categoryOnTxtList[id].SetActive(true);
    }

    public void Back() 
    {
        PageManager.instance.PageMove(1);
    }

    private void OnEnable()
    {
        for(int i = 0; i < categoryOnTxtList.Count; i++)
        {
            categoryOnTxtList[i].SetActive(false);   
        };

        categoryOnTxtList[0].SetActive(true);
    }

    private void OnDisable()
    {
        for(int i = 0; i < pageArray.Length; i++)
        {
            pageArray[i].SetActive(false);  
        };
    }
}
